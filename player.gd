extends KinematicBody2D


var speed = 200.0 # should be pixel / second

var health = 100.0

var oxygen = 80.0 # 100% of oxygen is good, means not suffocating 
var oxygen_absorb_rate = 4.0 # 1 no deltareally quick, 3 might be better
var oxygen_release_rate = -0.5 # needs to be slow release, possible based on facts later?
var oxygen_impacted_health = -4

var food = 80.0 # 100% of food means not hungry, 0 is bad
var food_depletion_rate = -0.5 # should be based on facts later
var food_impacted_health = -4

signal oxygen_changed(amount_changed) 
signal food_changed(amount_changed)
signal health_changed(amount_changed)
signal initial_status(init_oxygen, init_food, init_health)

func change_food(amount):
	food += amount
	if food < 0:
		food = 0
		change_health(food_impacted_health)
	if food > 100:
		food = 100.0
	
	emit_signal("food_changed", food)

func change_oxygen(amount):
	oxygen += amount
	if oxygen < 0:
		oxygen = 0
		change_health(oxygen_impacted_health)
	if oxygen > 100:
		oxygen = 100.0
	
	emit_signal("oxygen_changed", oxygen)
	
func change_health(amount):
	health += amount
	if health < 0:
		health = 0
	
	emit_signal("health_changed", health)

func _on_area_entered(area: CollisionObject2D): # this doesn't seem to be triggering
	# if food, we want to increase food
	# if oxygen, increase oxygen
	# if predator???, take damage? health?
	# if falling ice, take damange? health?
	print("area entered")
	if area.get_collision_layer() == 4: 
		change_oxygen(20)
		print(oxygen)

func _physics_process(delta):
	var direction = Vector2.ZERO
	var pot_collision = KinematicCollision2D
	
	if Input.is_action_pressed("ui_right"):
		direction.x += 1
		$AnimatedSprite.play("right-right") # Need to add
		
	if Input.is_action_pressed("ui_left"):
		direction.x -= 1
		$AnimatedSprite.play("left-left")
		
	if Input.is_action_pressed("ui_up"):
		direction.y -= 1
	if Input.is_action_pressed("ui_down"):
		direction.y += 1
	
	# Need to confirm two button press code below
	if direction.length() > 0:
		direction.normalized()
	
	if direction == Vector2.ZERO: 
		$AnimatedSprite.stop()
	
	# not supposed to update position directly aparently
	pot_collision = move_and_collide(direction * speed * delta)
	
	if pot_collision != null:
		if pot_collision.collider.get_collision_layer() == 4:# this worked!, returns the collision layer value
			#print("Oxygen Refilled")
			change_oxygen(oxygen_absorb_rate * delta) 
			#print(oxygen)
		
		if pot_collision.collider.get_collision_layer() == 8: # prey
			print("prey")
			pot_collision.collider.queue_free()
			change_food(10)

	if pot_collision == null:
		change_oxygen(oxygen_release_rate * delta)
		change_food(food_depletion_rate * delta)

	
# Called when the node enters the scene tree for the first time.
func _ready():
	emit_signal("initial_status", oxygen, food, health)
