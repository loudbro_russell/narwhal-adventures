extends KinematicBody2D


var base_speed = 50.0 # these things forage slowly
var fright_speed = 500.0 # but kick it into high gear when frightened


func _physics_process(delta):
	
	var direction = Vector2.RIGHT
	var _pot_collision = KinematicCollision2D
	$AnimatedSprite.play("right")
	
	
	_pot_collision = move_and_collide(direction * base_speed * delta)
	
	
