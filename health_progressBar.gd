extends TextureProgress




func _on_player_initial_status(init_oxygen, init_food, init_health):
	value = init_health


func _on_player_health_changed(amount_changed):
	value = amount_changed
