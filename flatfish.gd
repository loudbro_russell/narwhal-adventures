extends KinematicBody2D


var base_speed = 50.0 # these things forage slowly
var fright_speed = 500.0 # but kick it into high gear when frightened


func _physics_process(delta):
	
	var direction = Vector2.RIGHT
	var _pot_collision = KinematicCollision2D
	$AnimatedSprite.play("right")
	
	_pot_collision = move_and_collide(direction * base_speed * delta)
	
# Creating better movement
# should be able to use some math and basic masking principle to move to and from the player
# Vector2 is composed of x and y. Where the x and y determine the direction if positive /negative
# multiplying Vector2.y = 0.0 by any other vector2 will ensure that up and down movement don't happen (flatfish)
# manipulating the Vector2.x by switching between positive and negative can make the prey seem like it moves away from or toward the narwhal

