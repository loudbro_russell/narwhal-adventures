extends TextureProgress


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass


func _on_player_oxygen_changed(amount_changed):
	value = amount_changed


func _on_player_initial_status(init_oxygen, init_food, init_health):
	value = init_oxygen
	
